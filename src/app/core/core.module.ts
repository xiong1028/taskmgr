import { NgModule,SkipSelf,Optional } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
// 共享模块只加载一次
export class CoreModule { 
  //SkipSelf means igoring itself, ask app to go to the parent find CoreModule
  constructor(@Optional() @SkipSelf() parent:CoreModule){
    if(parent){
      throw new Error("Module is existed, it can not be reloaded");
    }

  }
}
